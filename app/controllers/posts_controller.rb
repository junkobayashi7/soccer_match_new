class PostsController < ApplicationController

before_action :set_post, only: [:show, :edit, :update, :destroy]





  def index
    @posts=Post.all
  end
  
  
  def show
    @post=Post.find_by(id: params[:id])
  end
  
  
  
  def new
  
  if !current_user
    redirect_to new_user_session_path
  else
    @post=Post.new
  end
 
  end
  
  
  
  def edit
    if @post.user != current_user
        redirect_to post_path(@post)
    end
  end
  
 
  
  
  
  
  def create
  @post = Post.new(post_params)
  @post.user_id = current_user.id
  respond_to do |format|
    if @post.save
      format.html { redirect_to @post, notice: 'Post was successfully created.' }
      format.json { render :show, status: :created, location: @post }
    else
      format.html { render :new }
      format.json { render json: @post.errors, status: :unprocessable_entity }
    end
  end
end



 def update
   respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  
  
  def destroy
      if @post.user == current_user   
        @post.destroy
      end 
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end





private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    def post_params
        params.require(:post).permit(:title,:event_date,:application_deadline,:holding_area,:venue,:capacity,:cost,:age,:longtext)
    end

end

