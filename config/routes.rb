Rails.application.routes.draw do

  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: "users/sessions",
    
   }
  
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
   
   
  get 'posts/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  
  resources :posts
  
  root 'home#top' 
end
